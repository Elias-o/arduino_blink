# Compte rendu Architecture Système Embarqué
> Voici un compte rendu des cours d'architecture système embarqué dans lequel nous avons eu une introduction plus ou moins poussée sur arduino ( programmation bare métal ).

## Matériel et logiciels utilisés
-  Arduino Uno Rev3 SMD (Boitier sur laquel était envoyés le code et qui faisait le rendu finale),
-  Machine Virtuelle, Linux Mint (Sous os Linux afin de pouvoir utiliser les fonctions linux et pouvoir accèder à tous ses bienfaits),
-  VSCodium (Visual Studio sur machine virtuelle avec des fonctionnalités utile pour ce que nous faisions), 
-  PlatformIO (Extensions de VS Codium qui va nous permettre de faire du développement spécifique Arduino),
-  GIT (Centraliser les différents commits et avancées du travail)

## Introduction aux logiciels
Lors de l'introduction au cours d'arduino, nous avons pu expérimenter les différents logiciels dont nous avons pu parler juste au dessus. Lors des essaies de ces différents logiciels une VM linux a été mis à notre disposition, Linux Mint, avec déja installés dessus VSCodium l'extension Platformio. On a pu donc grâce a cette extension appréhender plus facilement le code sur Arduino car les bases du projet étaient déja pré-ajoutées Ce fut un peu compliqué au départ pour comprendre les différents fonctions utiles et comment elles s'utilisaient nottament car il fallait comprendre comment s'utilisait le boitier arduino pour viser certains port et c'est vraiment beaucoup de connaissance complémentaire au code qu'il fallait et cette prise en main était plutôt compliqué au départ mais d'un point de vue éxterieur et surtout après le projet réalisé je me rends compte que avec de l'entrainemnt de la patience et de la rigueur on peut arriver à faire quelque chose de propre sur arduino. VSCodium n'était pas bien différent de VS Code que j'utilise beaucoup donc son fonctionnement n'était pas compliqué.

Nous avons également utilisé L'extension GIT, qui permet de versionner notre projet et ainsi d'avoir une meilleure gestion sur le développement et la maintenance de celui-ci.

## Projet "Blink"
Le projet "Blink" est une introduction à Arduino et à la programmation Bare métal (Programmation sans os). Pour ce projet nous avons utilisé une carte Arduino Uno, basée sur un ATMega328 cadencé à 16 MHz.

Pour la création du projet, nous utiliserons l'extension Platformio, dans la section "Board", nous sélectionnerons "Arduino Uno". Le projet va se créer seul et ajouter tout ce dont nous avons besoin pour faire un premier "Hello World!". Dans le dossier "src" du projet, un main.cpp est présent, c'est dans celui-ci que nous allons faire notre développement principale et c'est lui que le boitier Arduino va appeler lors de l'éxecution du programme. main.cpp est composé des fonction "setup()" et "loop()" qui sont les fonctions principales de notre programme (ajoutées automatiquement lors de la création du projet). La fonction "setup()" permet de paramétrer notre fichier et la fonction "loop()" est exécuter en boucle, suite à l'exécution du "setup()", lors l'exécution du projet.C'est en modifiant ces deux fonctions que l'on verra la programmation sur boitier arduino (et quelques autres que nous avons crées nous les verrons plus tard).

### Version 1
Tout d'abord nous nous sommes rendus sur le site officiel de Arduino directement dans la documentation nous avons chercher comment Configurer le boitier et d'utiliser grâçe aux ports le pin que nous voulions. Pour pouvoir réussir à manipuler le boitier nous avons trouvé deux fonctions utiles :
- pinMode(Pin, Mode), qui permet de configurer le pin que nous voulons utiliser et son mode par exemple "OUTPUT" en mode nous permettrait de le configurer en sortie
- digitalWrite(Pin, value), qui permet de changer la valeur du pin que nous aurions configuré au préalable avec le pinMode(); la value va permettre de donner une valeur à notre pin dans le cas d'une led, HIGH" premettrait d'allumer la led et la valeur "LOW" permettrait d'éteindre la led
- La fonction delay(), permet de faire une pause dans l'éxecution du code (en milisecondes), le mettre à 1000 le fait s'arrêter pendant 1 seconde. (A utiliser avec précaution car un delay() interrompt tout le code)
- La fonction "Serial.println("texte")" qui permet d'afficher dans le moniteur série la valeur "texte" à chaque exécution de la fonction loop(). Cela sert surtout de debugger dans le code pour savoir ou est-ce qu'on arrive avec la fonction, si il manque un des print que l'on a, cela veut dire que la fonction n'est pas arrivé à ce point.

Avec cela nous avons une première version du code qui compile et qui nous permet de faire une première version propre.
### Version 2
Maintenant nous n'allons plus utiliser les fonctions pinMode() et digitalWrite() de la doc arduino mais nous allons utiliser les ports et registres de notre carte Arduino UNO. Le DDR (Data Direction Register) est le registre qui détermine le sens de circulation des informations:
- Le sens en Entrée est 0
- Le sens en Sortie est 1
Le port va définir l'état de sortie (0 ou 1) et le pin va récupérer l'état d'entrée (0 ou 1).

Dans le datasheet de arduino Uno, nous pouvons voir que le pin 13 qui nous intéresse (la led) est situé sur le port PB5. Donc pour pouvoir allumer cette led, il faut passer à 1 le bits qui correspond à ce port donc le bit 5 du port B on met donc à 1 le bit 5.

Pour gérer la LED, nous avons crée deux fonctions : led_on() et led_off(), 
Lorsque nous appelons led_on() nous mettons le bit 5 à 1 et lorsque l'on appelle la fonction led_off() qui elle va faire une sorte d'inversion de mettre des 1 partout cela veut dire que tout ce qui est à 0 reste à zéros et les 1 restent des 1 sauf celui du bit 5, la led va donc s'éteindre.

Cette utilisation va donc remplacer les deux fonctions pinMode() qui configurait le bon pin et digitalWrite qui allumait et éteignez la led.
### Version 3
Dans cette version nous avons simplement automatisé les transitions d'états de la led, au lieu d'avoir deux fonctions un led_off() et un led_on() nous avons une fonction led_toggle() qui va gérer à elle seule les deux états de la led cela rend le code beaucoup plus lisible et il y aura beaucoup moins d'appel de fonction.

### Version 4
Depuis le début du développement autour de arduino nous utilisons la fonction Delay() celle ci qui permet d'interrompre le code pendant le temps que l'on désire, nous avons donc décidé de reproduire un timer identique mais en comprenant ce qui se passe lors de son éxecution.

La plus part des choses sont écrites dans le code mais faisons un rapide résumé :

On commence par le paramétrage de notre PRESCALER, CS_PRESCALER_1024 ((1 << CS10) | (1 << CS12)), qui correspondant à deux bits du registre TCCR1B qui vont nous permettre de gérer le timer,
- Comprendre l'utilisation du timer : 
Capacité 16 bits -> possibilité de coder 65 535 valeurs donc beaucoup plus large en termes de valeurs.
- Comment ça fonctionne ?
    -  Registre TCNTn va contenir la valeur incrémentée.
- Pour assembler les valeurs de notre timer :

Configuration
- Prescaler/Clock
- Valeur de comparaison
- Mode
- Activation interruption


Configuration
- Prescaler/Clock  TCCR1B : bit 0 à 2 
- Mode :
    - TCCR1B (WGM13 et WGM12) --> bit 3 et 4 // WG13 à 0, WGM12 à 1
    - TCCR1A (WGM11 et WGM10) --> bit 1 et 2 OCIE1A bit 1
- Valeur Comparaison --> OCR1AH et OCR1AL
- Activation interruption --> TIMSK1 bit 1

> Pour plus de lisibilité dans le code il est possible d’ajouter des define afin d’avoir des choses plus lisibles et compréhensible,
Il est également possible d’ajouter un prescaler en define,

# Conclusion
Lors de ces cours/TP, nous avons pu voir tout ce qui concernait l'introduction à Arduino nous avons vu un exemple d'utilisation d'un pin avec plusieurs méthodes et moyens pour pouvoir tous les cas d'utilisation des ports et pins arduino. Grâce à ce que l'on a vu nous pouvons manipuler n'importe quel port du boitier arduino et donc utiliser tous les pins disponibles. Associés avec d'autres modules cela laisse libre choix à l'imagination sur ce qui est faisable en arduino.

> Pour la partie Linux et RaspBerry, j'étais en arrêt du au covid et n'ai donc pas pu participer au cours, veuillez m'excuser pour la gêne.


